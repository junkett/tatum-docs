# TATUM.IO - David Bartos - Job application task

## General information

This readme is providing general information about architecture, decisions, implementation of the small project for Tatum blockchain

## Task Requirements

### Goal

Demonstrate the ability to automate the deployment of a dockerized application using Gitlab CI  
Infrastructure and tools:

* AWS EC2 (or other AWS serverless service)
* AWS S3
* Docker and your preferred docker image
* Ansible / Terraform
* Python

1. Download regularly (e.g. daily / hourly) some dataset from a free data provider. If you don’t
know any, choose from:
https://github.com/CSSEGISandData/COVID-19
https://openweathermap.org/current
2. Store downloaded dataset to an S3 bucket
3. From every downloaded dataset, extract some specific data (e.g. data relevant to Czechia,
Prague..)
4. Display all extracted data using a single HTML page served from S3. A simple table is
enough.
5. Present the result

### Instructions:

Use a well-known language (preferably Python3) to create the application  
Create a docker container to encapsulate the application logic  
Use the latest ansible / terraform to automate the provisioning of the AWS resources  
Use Gitlab to:  
Store all your source code including a Readme.MD which will explain your approach  
Document your whole development process within one Merge request  
Use Gitlab CI to automate the deployment of the application to your AWS environment  
Document all problems / todos which you encountered during the implementation  

### Bonus points:

Make the application highly available  
Replace simple docker with a container orchestrator  

## Gitlab reference

All the data is stored in GitLab under my tenant "junkett" in 6 projects.  
I had decided to split the different parts of the deployment into different projects as in my opinion it is more clear to use them separately and they should not interfere with each other.

1. Ansible - all needed stuff related to Ansible config management. https://gitlab.com/junkett/ansible  
2. Extract App - python application, docker image and k8s deployment. https://gitlab.com/junkett/extract-app  
3. Image Repo - repository with 2 containers - Python app and Tools. https://gitlab.com/junkett/App  
4. Terraform - all needed stuff for Terraform IaC. https://gitlab.com/junkett/terraform  
5. Tools - Dockerfile that is used to build docker image (Centos8 based) to run the Ansible and Terraform etc. https://gitlab.com/junkett/tools  
6. Tatum Docs - This Readme file only, common for all projects. https://gitlab.com/junkett/tatum-docs/-/blob/main/README.md

## Components and Architecture

I had considered few different approaches to this and decided to get some bonus points and use k0s kubernetes to provide orchestration and high availability. To support high availablility I had deployed environment into multiple availability zones.  
I understand this is not full HA since there is only single k8s controller but this could be scaled easily in case of need.  
I had also tried to stay within free tier.  

### App Access

The application is available on http://pocasi.bartosovic.cz (a dns cname record pointing to http://pocasi.bartosovic.cz.s3-website.eu-central-1.amazonaws.com)

### Security

All static sensitive data are stored in GitLab variables and passed on via GitLab Pipeline as environment variables.  
In case of dynamic data such as Kubeconfig or Ansible inventory are stored after Ansible k0s cluster deployment in AWS Secrets Manager and then pulled from there when needed.  
Security groups are limiting access to specific ports from outside only to Bastion/Controller node.  

Following secrets must be stored in specified GitLab projects variables:  
* AWS_SEC: AWS Secret key for authentication of aws cli - Projects Ansible, Extract App
* AWS_KEY: AWS Access key for authentication of aws cli - Projects Ansible, Extract App
* DOCK_PASS: API token for pulling Image Repo stored images - Projects Ansible, Extract App
* push_token: API token for pushing images to Image Repo - Projects Tools, Extract App
* SSH_KEY: SSH key to access git repositories - Projects Ansible, Extract App
* SSH_KEY_AWS: SSH key for accessing EC2 instances - Projects Ansible
* AWS_SECRET_ACCESS_KEY: AWS Secret key for authentication of aws cli - Projects Terraform
* AWS_ACCESS_KEY_ID: AWS Access key for authentication of aws cli - Projects Terraform

### Terraform Infrastructure

Terraform deployed in Frankfurt AWS region - statefile stored in S3 bucket 
Code repository: https://gitlab.com/junkett/terraform  
This is driven by pipeline: https://gitlab.com/junkett/terraform/-/pipelines  
* VPC with 3 subnets, each in one availability zone, Internet gateway.
* 3 instances deployed, each in different availability zone (1 controller and 2 workers).
* Instances have assigned public IPs - I had been considering use of NAT GW but that is a paid resource, also would be possible to configure some forward proxy to provide html proxy function for backend nodes, but this would complicate whole architecture to maintain HA so that is why I had decided to use just EIPs on all instances.
* Security groups only allowing ssh and control plane connection on TCP 6443 to K0S controller.
* Workers have only connection through controller.
* Outbound traffic allowed for all instances.
* 2 S3 buckets deployed.
* Bucket that is not publicly accessible that stores datasets in gz format downloaded from Openweather.
* Bucket that is publicly accessible and has static website enabled to provide webserver to display data extracted from large dataset (Ostrava weather data).

__Pipeline details__

Pipeline is based on default template for Terraform in Gitlab

__TO-DOs__

This can be modularised by developing module to provision EC2 instances together with nics etc.  
Enhance the HA deployment - if needed change the recipe to be able to deploy multiple resources of controller and worker nodes. Could be easily done if module is created and then use count attribute.

### Configuration mgmt

Ansible taking care of config mgmt on all EC2 instances.  
I had used k0s-ansible project as a base for my deployment: https://github.com/movd/k0s-ansible  
And customized it to provide functionality I needed.  
Code repository: https://gitlab.com/junkett/ansible  
This is driven by pipeline: https://gitlab.com/junkett/ansible/-/pipelines  

__Ansible Roles__

* Files - Creates inventory and updates ssh config to be able to manage new instances.
* Site - Configures k0s cluster, provides kubectl config. I had to do some modifications to be able to run configurations through controller as a bastion.
* Deploy-app - Deploys application to k0s cluster (kubectl create repository secret so it can pull the image and then deploys python app image to k0s cluster)
* Reset - Removes k0s cluster so it can be re-deployed

__Pipeline details__

To be able to run this dynamically the pipeline is using prescript to download all needed repos, configure basic ssh config  
Then executes roles in following order > Files > Reset > Site > collects kubeconfig as an artifact after deployment  

### Python app

Application that extracts data from web source and provide them to S3 for storage and static webapp.  
Included is also pipeline that builds Docker image and deploys to k8s.  
Code repository: https://gitlab.com/junkett/extract-app  

This is a small application that does following:  

* Downloads dataset from Openweather (using just a sample dataset)
* Extracts the dataset (gz archive)
* Extract data by searching for specific City string in the payload (Ostrava) - this is a json data.
* Creates a html page with table from that json
* Uploads original dataset to S3 bucket
* Uploads extracted dataset html to S3 webapp bucket
* Uses parameters from environment variables to specify buckets and city that we want to extract data for.

__K8S usage__

There is a Docker image built and stored in Image repo (registry.gitlab.com/junkett/app/app) which has application encapsulated.  
Docker image is automatically built upon changes - driven by pipeline: https://gitlab.com/junkett/extract-app/-/pipelines  
Application is intended to be run on schedule via K8S Cron job every hour. It is not needed to be running all the time from nature of activities that it handles.  

__High Availability__

HA is provided by K8S that it can run the job on available worker node. It is set to restart if fails during execution so in case of K8S worker node crashes during the execution it should be immediately restarted and finish its job. There is no issue if the pod crashes during its execution as next run will collect new data and provide them on static web app

__TO-DOs__

Logging can be more talkative (currently if all ok the app is completely silent:)  

## Main issues encountered along the development

There were some hiccups along the way so here are main ones also with information how I had resolved them:

### Terraform

Overall this was quite small deployment, main challenge was with providing TF output to Ansible. Perhaps could be handled by providing artifact from terraform pipeline to ansible pipeline (to use native Gitlab function, to do that I need to do some more investigation on the process), but finally got that done by executing the tf output during ansible run and collecting the results there.

### Ansible

Generally an issue with k0s version - as the project was deploying older k0s and I decided to use newer (1.22 initially), which seem to suffer from some bugs (some pods were not able to start). Tried to troubleshoot that but then tried latest 1.21 version and that worked well.  
I had some issues with ssh accessing the backend environment via controller, finally that was working after I had provided ssh key to bastion, this was odd as I would expect the proxy command on ssh would normally work. I have to investigate this bit further.  

### Docker Images

Some minor issues with registry image pushing - as I did not realize that initially did create registry api key for reading content to be able to deploy the app in k8s - this got fixed by ceating another key with write permissions obviously:)

### K8S

I had been considering also use of webserver based on Nginx and ELB but that seem to be really complicating things and difficult for me to develop in short term. Expecially when there is static web app function availability on S3. The idea was to provide ability to use tls etc...

### Python App

Main issue was with not including the ContentType:text/html, this had caused the page to offering download of the index.html instead present the web page. Then even it should been fixed the browser was still offering download - but I had found out that this is some browser cache issue as once I used anonymous session it worked fine.

### Gitlab

Generally I have to admit that I am not that much familiar with Gitlab (been working with other tools like Azure Devops, Jenkins, GoCD), so it was all mostly learning by doing for me.  

I had to get used to situation when there is single pipeline per project for example.  
I am still getting to understand all the possibilities on how the pipelines work but got familiar enough to be able to automate the stuff I need. It seems as well integrated all in one devops tool. Especially like that I do not need to handle authentication when executing the pipeline and using resources from GitLab.  
Main issue for me was to find efficient way of passing artifacts like inventory or kubeconfig from one pipeline / project to another.
